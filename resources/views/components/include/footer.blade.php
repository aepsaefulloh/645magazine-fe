<div class="footer footer-6 mt-3 mt-lg-4">
    <div class="footer-top clearfix text-center text-uppercase">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 newsletter-col mt-0 mb-4">
                    <form action="#" method="post" class="footer-newsletter mb-3">
                        <label class="h4 mb-3 pb-1 fw-bold text-white">SIGN UP and get 20% off coupon for all
                            items</label>
                        <div class="input-group">
                            <input type="email" class="border-0 input-group__field newsletter-input rounded-0"
                                name="EMAIL" value="" placeholder="Email address" required>
                            <span class="input-group__btn"><button type="submit"
                                    class="btn btn-secondary newsletter__submit rounded-0"
                                    name="commit">Subscribe</button></span>
                        </div>
                    </form>
                    <ul class="list-inline social-icons justify-content-center">
                        <li class="list-inline-item"><a class="text-decoration-none" href="#" data-bs-toggle="tooltip"
                                data-bs-placement="top" title="Facebook"><i class="an an-facebook"
                                    aria-hidden="true"></i></a></li>
                        <li class="list-inline-item"><a class="text-decoration-none" href="#" data-bs-toggle="tooltip"
                                data-bs-placement="top" title="Twitter"><i class="an an-twitter"
                                    aria-hidden="true"></i></a></li>
                        <li class="list-inline-item"><a class="text-decoration-none" href="#" data-bs-toggle="tooltip"
                                data-bs-placement="top" title="Pinterest"><i class="an an-pinterest-p"
                                    aria-hidden="true"></i></a></li>
                        <li class="list-inline-item"><a class="text-decoration-none" href="#" data-bs-toggle="tooltip"
                                data-bs-placement="top" title="Instagram"><i class="an an-instagram"
                                    aria-hidden="true"></i></a></li>
                        <li class="list-inline-item"><a class="text-decoration-none" href="#" data-bs-toggle="tooltip"
                                data-bs-placement="top" title="TikTok"><i class="an an-tiktok"
                                    aria-hidden="true"></i></a></li>
                        <li class="list-inline-item"><a class="text-decoration-none" href="#" data-bs-toggle="tooltip"
                                data-bs-placement="top" title="Whatsapp"><i class="an an-whatsapp"
                                    aria-hidden="true"></i></a></li>
                    </ul>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 mb-4">
                    <ul class="list-inline mb-0 inline-links d-flex-center justify-content-center">
                        <li class="list-inline-item p-1"><a href="my-account.html">My Account</a></li>
                        <li class="list-inline-item p-1"><a href="#">Support Center</a></li>
                        <li class="list-inline-item p-1"><a href="#">Terms &amp; condition</a></li>
                        <li class="list-inline-item p-1"><a href="#">Returns &amp; Exchanges</a></li>
                        <li class="list-inline-item p-1"><a href="#">Shipping &amp; Delivery</a></li>
                    </ul>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="copytext">&copy; 2022 Optimal. All Rights Reserved.</div>
                </div>
            </div>
        </div>
    </div>

</div>