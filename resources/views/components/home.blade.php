@extends('master')
@section('body_class', 'template-index index-demo6 modal-popup-style')

@section('content')
<!--Home Video-->
<section class="slideshow slideshow-wrapper p-0">
    <div class="home-slideshow">
        <div class="slide slide-video slide-media">
            <img src="{{asset('assets')}}/images/slideshow/slide-1.jpg" class="img-fluid" alt="">
            <div class="container">
                <div
                    class="slideshow-content slideshow-overlay middle d-flex justify-content-center align-items-center">
                    <div class="slideshow-content-in text-center">
                        <div class="wrap-caption animation style1 whiteText px-2">
                            <h1 class="h1 mega-title ss-mega-title fs-1 text-capitalize">LIFE IS GOOD
                                <br>ENJOY THE RIDE
                            </h1>
                            <span class="mega-subtitle fs-6 ss-sub-title d-md-block d-lg-block d-none">WEAR
                                FREEDOM ADVENTURE</span>
                            <!-- <div class="ss-btnWrap d-flex-center justify-content-center mt-3 mt-md-0">
                                <a class="btn btn-lg border-0 btn-primary" href="shop-left-sidebar.html">SHOP NOW</a>
                                <a class="btn btn-lg border-0 btn-primary" href="shop-left-sidebar.html">LEARN MORE</a>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Home Video-->

<section class="section banner-text">
    <div class="container">
        <div class="section-header m-0 col-xl-9 mx-auto">
            <h2>Now it’s time to Leave the Main Road</h2>
            <p class="my-3 ls-base">We encourage you not to follow others or do the same things as everyone. Explore the
                world, live life, take risks and map new paths.</p>
        </div>
    </div>
</section>

<section class="section collection-banners style7 pt-2">
    <div class="container-fluid px-4">
        <div class="row mx-0">
            <div class="col-md-6 cl-item banner-item item">
                <div class="collection-grid-item">
                    <a href="shop-top-filter.html">
                        <div class="img"><img class="blur-up lazyload"
                                data-src="{{asset('assets')}}/images/collection/collection-1.jpg"
                                src="{{asset('assets')}}/images/collection/collection-1.jpg" alt="collection"
                                title="" /></div>
                        <div class="details bottom bg-transparent">
                            <div class="inner col-11 col-lg-10 col-xxl-8 py-3">
                                <h3 class="title fw-normal lh-1 mb-0">APPAREL</h3>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-6 cl-item banner-item item">
                <div class="collection-grid-item">
                    <a href="shop-top-filter.html">
                        <div class="img"><img class="blur-up lazyload"
                                data-src="{{asset('assets')}}/images/collection/collection-2.jpg"
                                src="{{asset('assets')}}/images/collection/collection-2.jpg" alt="collection"
                                title="" /></div>
                        <div class="details bottom bg-transparent">
                            <div class="inner col-11 col-lg-10 col-xxl-8 py-3">
                                <h3 class="title fw-normal lh-1 mb-0">ACCESSORIES</h3>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-6 cl-item banner-item item">
                <div class="collection-grid-item">
                    <a href="shop-top-filter.html">
                        <div class="img"><img class="blur-up lazyload"
                                data-src="{{asset('assets')}}/images/collection/collection-3.jpg"
                                src="{{asset('assets')}}/images/collection/collection-3.jpg" alt="collection"
                                title="" /></div>
                        <div class="details bottom bg-transparent">
                            <div class="inner col-11 col-lg-10 col-xxl-8 py-3">
                                <h3 class="title fw-normal lh-1 mb-0">GLOVES</h3>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-6 cl-item banner-item item">
                <div class="collection-grid-item">
                    <a href="shop-top-filter.html">
                        <div class="img"><img class="blur-up lazyload"
                                data-src="{{asset('assets')}}/images/collection/collection-4.jpg"
                                src="{{asset('assets')}}/images/collection/collection-4.jpg" alt="collection"
                                title="" /></div>
                        <div class="details bottom bg-transparent">
                            <div class="inner col-11 col-lg-10 col-xxl-8 py-3">
                                <h3 class="title fw-normal lh-1 mb-0">JERSEY</h3>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section product-slider tab-slider-product">
    <div class="container">
        <div class="section-header">
            <h2 class="text-transform-none">Special Offers</h2>
        </div>

        <div class="tabs-listing">
            <ul class="tabs clearfix tabs-style1">
                <li class="active" rel="tab1">Jackets</li>
                <li rel="tab2">Pants</li>
            </ul>
            <div class="tab_container">
                <h3 class="tab_drawer_heading d_active body-font text-uppercase text-center" rel="tab1">Exciting Offers
                    For You <i class="an an-angle-down-r" aria-hidden="true"></i></h3>
                <div id="tab1" class="tab_content grid-products">
                    <div class="row">
                        @for($i=1; $i < 9; $i++) <div class="item col-lg-3 col-md-4 col-6">
                            <!--Start Product Image-->
                            <div class="product-image">
                                <!--Start Product Image-->
                                <a href="#" class="product-img">
                                    <!--Image-->
                                    <img class="primary blur-up lazyload"
                                        data-src="{{asset('assets')}}/images/products/product-1.jpg"
                                        src="{{asset('assets')}}/images/products/product-1.jpg" alt="product" title="">
                                    <!--End Image-->
                                    <!--Hover Image-->
                                    <img class="hover blur-up lazyload"
                                        data-src="{{asset('assets')}}/images/products/product-1.jpg"
                                        src="{{asset('assets')}}/images/products/product-1.jpg" alt="product" title="">
                                    <!--End Hover Image-->
                                </a>
                                <!--End Product Image-->
                                <!--Product Button-->
                                <div class="button-set style2 d-none d-md-block">
                                    <ul>
                                        <!--Cart Button-->
                                        <li><a class="btn-icon btn cartIcon pro-addtocart-popup"
                                                href="#pro-addtocart-popup"><i class="icon an an-cart-l"></i> <span
                                                    class="tooltip-label top">Add to Cart</span></a></li>
                                        <!--End Cart Button-->
                                        <!--Quick View Button-->
                                        <li><a class="btn-icon quick-view-popup quick-view" href="javascript:void(0)"
                                                data-toggle="modal" data-target="#content_quickview"><i
                                                    class="icon an an-search-l"></i> <span
                                                    class="tooltip-label top">Quick View</span></a></li>
                                        <!--End Quick View Button-->
                                        <!--Wishlist Button-->
                                        <li><a class="btn-icon wishlist add-to-wishlist" href="my-wishlist.html"><i
                                                    class="icon an an-heart-l"></i> <span class="tooltip-label top">Add
                                                    To Wishlist</span></a></li>
                                        <!--End Wishlist Button-->
                                        <!--Compare Button-->
                                        <li><a class="btn-icon compare add-to-compare" href="compare-style2.html"><i
                                                    class="icon an an-sync-ar"></i> <span class="tooltip-label top">Add
                                                    to Compare</span></a></li>
                                        <!--End Compare Button-->
                                    </ul>
                                </div>
                                <!--End Product Button-->
                            </div>
                            <!--End Product Image-->
                            <!--Start Product Details-->
                            <div class="product-details text-start">
                                <!--Product Name-->
                                <div class="product-name">
                                    <a href="#" class="fw-normal">Product Name</a>
                                </div>
                                <!--End Product Name-->
                                <!--Product Price-->
                                <div class="product-price">
                                    <span class="price">Rp. 250.000</span>
                                </div>
                                <!--End Product Price-->

                            </div>
                            <!--End Product Details-->
                    </div>
                    @endfor
                </div>

                <div class="view-collection text-center mt-3 mt-md-4">
                    <a href="#" class="btn btn-lg rounded-0">View All</a>
                </div>
            </div>

            <h3 class="tab_drawer_heading body-font text-uppercase text-center" rel="tab2">Most Popular <i
                    class="an an-angle-down-r" aria-hidden="true"></i></h3>
            <div id="tab2" class="grid-products tab_content">
                <div class="row">
                    @for($i=1; $i < 9; $i++) <div class="item col-lg-3 col-md-4 col-6">
                        <!--Start Product Image-->
                        <div class="product-image">
                            <!--Start Product Image-->
                            <a href="#" class="product-img">
                                <!--Image-->
                                <img class="primary blur-up lazyload"
                                    data-src="{{asset('assets')}}/images/products/product-1.jpg"
                                    src="{{asset('assets')}}/images/products/product-1.jpg" alt="product" title="">
                                <!--End Image-->
                                <!--Hover Image-->
                                <img class="hover blur-up lazyload"
                                    data-src="{{asset('assets')}}/images/products/product-1.jpg"
                                    src="{{asset('assets')}}/images/products/product-1.jpg" alt="product" title="">
                                <!--End Hover Image-->
                            </a>
                            <!--End Product Image-->
                            <!--Product Button-->
                            <div class="button-set style2 d-none d-md-block">
                                <ul>
                                    <!--Cart Button-->
                                    <li><a class="btn-icon btn cartIcon pro-addtocart-popup"
                                            href="#pro-addtocart-popup"><i class="icon an an-cart-l"></i> <span
                                                class="tooltip-label top">Add to Cart</span></a></li>
                                    <!--End Cart Button-->
                                    <!--Quick View Button-->
                                    <li><a class="btn-icon quick-view-popup quick-view" href="javascript:void(0)"
                                            data-toggle="modal" data-target="#content_quickview"><i
                                                class="icon an an-search-l"></i> <span class="tooltip-label top">Quick
                                                View</span></a></li>
                                    <!--End Quick View Button-->
                                    <!--Wishlist Button-->
                                    <li><a class="btn-icon wishlist add-to-wishlist" href="my-wishlist.html"><i
                                                class="icon an an-heart-l"></i> <span class="tooltip-label top">Add
                                                To Wishlist</span></a></li>
                                    <!--End Wishlist Button-->
                                    <!--Compare Button-->
                                    <li><a class="btn-icon compare add-to-compare" href="compare-style2.html"><i
                                                class="icon an an-sync-ar"></i> <span class="tooltip-label top">Add
                                                to Compare</span></a></li>
                                    <!--End Compare Button-->
                                </ul>
                            </div>
                            <!--End Product Button-->
                        </div>
                        <!--End Product Image-->
                        <!--Start Product Details-->
                        <div class="product-details text-start">
                            <!--Product Name-->
                            <div class="product-name">
                                <a href="#" class="fw-normal">Product Name</a>
                            </div>
                            <!--End Product Name-->
                            <!--Product Price-->
                            <div class="product-price">
                                <span class="price">Rp. 345.000</span>
                            </div>
                            <!--End Product Price-->

                        </div>
                        <!--End Product Details-->
                </div>
                @endfor
            </div>

            <div class="view-collection text-center mt-3 mt-md-4">
                <a href="#" class="btn btn-lg rounded-0">View All</a>
            </div>
        </div>
    </div>
    </div>
    </div>
</section>

<!--Parallax Banner-->
<div class="section parallax-banner-style1 py-0">
    <div class="hero bgFixed hero__overlay bg-size">
        <img class="bg-img" src="{{asset('assets')}}/images/slideshow/banner-parallax.jpg" alt="parallax-banner" />
        <div class="hero__inner">
            <div class="container">
                <div class="wrap-text center text-large font-bold mw-100">
                    <h1 class="mega-title text-transform-none text-white">About Us</h1>
                    <div class="h2 mega-subtitle text-transform-none text-white mb-3">645 Magazine</div>
                    <p class="details text-white d-none d-md-block">Dressing up your kiddos in cute outfits is always
                        fun!<br> High shine, long-lasting, vegan nail polishes in arange of <br> fashion colours and
                        finishes</p>
                    <a href="shop-right-sidebar.html" class="btn btn-white">Discover More</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End Parallax Banner-->

<section class="section home-blog-post">
    <div class="container">
        <div class="section-header">
            <h2>Motorcycle News</h2>
            <p>TOP NEWS STORIES OF THE DAY</p>
        </div>

        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                <div class="blog-post-slider text-center">
                    @for($i=1; $i < 4; $i++) <div class="blogpost-item mb-0">
                        <a href="#" class="post-thumb"><img class="blur-up lazyload"
                                src="https://645magazine.com/upload/article/1656929955.jpg"
                                data-src="https://645magazine.com/upload/article/1656929955.jpg" alt="blog" title="" /></a>
                        <div class="post-detail text-center">
                            <h4 class="post-title"><a href="#">The Brotos Vespa Race Seri II | Ada Balap Vespa Tahun 50-an </a>
                            </h4>
                            <ul class="publish-detail d-flex-center justify-content-center">
                                <li class="d-flex align-items-center"><i class="an an-calendar me-2 d-none"></i> <span
                                        class="article__date">March 06, 2022</span></li>
                                <li class="d-flex align-items-center"><i class="an an-user-expand me-2 d-none"></i>
                                    <span class="article__author">by 654</span>
                                </li>
                                <li class="d-flex align-items-center"><i class="an an-comments-l me-2 d-none"></i> <a
                                        href="#;" class="article-link">14 comment</a></li>
                            </ul>
                        </div>
                </div>
                @endfor

            </div>
        </div>
    </div>
    </div>
</section>

<div class="section home-instagram">
    <div class="container">
        <div class="section-header">
            <h2>Follow Us On Instagram</h2>
            <p>@654magazine</p>
        </div>

        <!--Instagram Grid-->
        <div class="instagram-grid instagram-grid-style3 clearfix">
            <ul class="clearfix">
                <li class="instagram-item">
                    <a href="#;">
                        <img src="{{asset('assets')}}/images/instagram/ins-9.jpg"
                            data-src="{{asset('assets')}}/images/instagram/ins-9.jpg" alt="image" title=""
                            class="blur-up lazyload" />
                        <span class="ins-icon"><i class="icon an an-instagram"></i></span>
                    </a>
                </li>
                <li class="instagram-item">
                    <a href="#;">
                        <img src="{{asset('assets')}}/images/instagram/ins-10.jpg"
                            data-src="{{asset('assets')}}/images/instagram/ins-10.jpg" alt="image" title=""
                            class="blur-up lazyload" />
                        <span class="ins-icon"><i class="icon an an-instagram"></i></span>
                    </a>
                </li>
                <li class="instagram-item">
                    <a href="#;">
                        <img src="{{asset('assets')}}/images/instagram/ins-11.jpg"
                            data-src="{{asset('assets')}}/images/instagram/ins-11.jpg" alt="image" title=""
                            class="blur-up lazyload" />
                        <span class="ins-icon"><i class="icon an an-instagram"></i></span>
                    </a>
                </li>
                <li class="instagram-item">
                    <a href="#;">
                        <img src="{{asset('assets')}}/images/instagram/ins-12.jpg"
                            data-src="{{asset('assets')}}/images/instagram/ins-12.jpg" alt="image" title=""
                            class="blur-up lazyload" />
                        <span class="ins-icon"><i class="icon an an-instagram"></i></span>
                    </a>
                </li>
                <li class="instagram-item">
                    <a href="#;">
                        <img src="{{asset('assets')}}/images/instagram/ins-13.jpg"
                            data-src="{{asset('assets')}}/images/instagram/ins-13.jpg" alt="image" title=""
                            class="blur-up lazyload" />
                        <span class="ins-icon"><i class="icon an an-instagram"></i></span>
                    </a>
                </li>
                <li class="instagram-item">
                    <a href="#;">
                        <img src="{{asset('assets')}}/images/instagram/ins-14.jpg"
                            data-src="{{asset('assets')}}/images/instagram/ins-14.jpg" alt="image" title=""
                            class="blur-up lazyload" />
                        <span class="ins-icon"><i class="icon an an-instagram"></i></span>
                    </a>
                </li>
                <li class="instagram-item">
                    <a href="#;">
                        <img src="{{asset('assets')}}/images/instagram/ins-15.jpg"
                            data-src="{{asset('assets')}}/images/instagram/ins-15.jpg" alt="image" title=""
                            class="blur-up lazyload" />
                        <span class="ins-icon"><i class="icon an an-instagram"></i></span>
                    </a>
                </li>
                <li class="instagram-item">
                    <a href="#;">
                        <img src="{{asset('assets')}}/images/instagram/ins-16.jpg"
                            data-src="{{asset('assets')}}/images/instagram/ins-16.jpg" alt="image" title=""
                            class="blur-up lazyload" />
                        <span class="ins-icon"><i class="icon an an-instagram"></i></span>
                    </a>
                </li>
                <li class="instagram-item">
                    <a href="#;">
                        <img src="{{asset('assets')}}/images/instagram/ins-17.jpg"
                            data-src="{{asset('assets')}}/images/instagram/ins-17.jpg" alt="image" title=""
                            class="blur-up lazyload" />
                        <span class="ins-icon"><i class="icon an an-instagram"></i></span>
                    </a>
                </li>
                <li class="instagram-item">
                    <a href="#;">
                        <img src="{{asset('assets')}}/images/instagram/ins-4.jpg"
                            data-src="{{asset('assets')}}/images/instagram/ins-4.jpg" alt="image" title=""
                            class="blur-up lazyload" />
                        <span class="ins-icon"><i class="icon an an-instagram"></i></span>
                    </a>
                </li>
                <li class="instagram-item">
                    <a href="#;">
                        <img src="{{asset('assets')}}/images/instagram/ins-2.jpg"
                            data-src="{{asset('assets')}}/images/instagram/ins-2.jpg" alt="image" title=""
                            class="blur-up lazyload" />
                        <span class="ins-icon"><i class="icon an an-instagram"></i></span>
                    </a>
                </li>
                <li class="instagram-item">
                    <a href="#;">
                        <img src="{{asset('assets')}}/images/instagram/ins-6.jpg"
                            data-src="{{asset('assets')}}/images/instagram/ins-6.jpg" alt="image" title=""
                            class="blur-up lazyload" />
                        <span class="ins-icon"><i class="icon an an-instagram"></i></span>
                    </a>
                </li>
            </ul>
        </div>
        <!--End Instagram Grid-->
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 mt-3 mt-sm-4 text-center">
                <a href="#;" class="btn btn-primary text-transform-none">View Gallery</a>
            </div>
        </div>
    </div>
</div>
<!--End Instagram Section-->
</div>

@endsection