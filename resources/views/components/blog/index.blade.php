@extends('master')
@section('body_class', 'template-index')

@section('content')
<!--Home Video-->
<section class="slideshow slideshow-wrapper p-0">
    <div class="home-slideshow">
        <div class="slide slide-video slide-media">
            <img src="{{asset('assets')}}/images/slideshow/banner-page.jpg" class="img-fluid" alt="">
            <div class="container">
                <div
                    class="slideshow-content slideshow-overlay middle d-flex justify-content-center align-items-center">
                    <div class="slideshow-content-in text-center">
                        <div class="wrap-caption animation style1 whiteText px-2">
                            <h1 class="h1 mega-title ss-mega-title fs-1 text-capitalize">LIFE IS GOOD
                                <br>ENJOY THE RIDE
                            </h1>
                            <span class="mega-subtitle fs-6 ss-sub-title d-md-block d-lg-block d-none">WEAR
                                FREEDOM ADVENTURE</span>
                            <!-- <div class="ss-btnWrap d-flex-center justify-content-center mt-3 mt-md-0">
                                <a class="btn btn-lg border-0 btn-primary" href="shop-left-sidebar.html">SHOP NOW</a>
                                <a class="btn btn-lg border-0 btn-primary" href="shop-left-sidebar.html">LEARN MORE</a>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Home Video-->
<!--Collection Banner-->
<div class="collection-header">
    <div class="collection-hero">
        <div class="collection-hero__image"></div>
        <div class="collection-hero__title-wrapper container">
            <h1 class="collection-hero__title">Article</h1>
            <div class="breadcrumbs text-uppercase mt-1 mt-lg-2"><a href="{{url('/')}}"
                    title="Back to the home page">Home</a><span>|</span><span class="fw-bold">Article</span>
            </div>
        </div>
    </div>
</div>
<!--End Collection Banner-->

<div class="container">
    <div class="row">
        <!--Main Content-->
        <div class="col-12 col-sm-12 col-md-12 col-lg-12 main-col">
            <div class="blog--list-view blog-grid-view no-border">
                <div class="row">
                    @for($i = 0; $i < 6; $i++)
                    <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                        <div class="article">
                            <div class="row">
                                <!-- Article Image -->
                                <div class="post-img">
                                    <a class="article_featured-image zoom-scal" href="#"><img class="blur-up lazyload"
                                            data-src="{{asset('assets')}}/images/blog/post-1.jpg"
                                            src="{{asset('assets')}}/images/blog/post-1.jpg"
                                            alt="Unique Designs, Unique You">
                                    </a>
                                </div>
                                <!-- Article Content -->
                                <div class="post-content">
                                    <h2 class="h3 text-transform-none"><a href="#">Unique Designs,
                                            Unique You</a></h2>
                                    <ul class="publish-detail d-flex-wrap">
                                        <li><i class="icon an an-clock-r"></i><time datetime="2021-01-28">January 28,
                                                2021</time></li>
                                        <li><i class="icon an an-user-al"></i><span class="clr-555 me-1">Posted
                                                by:</span>User</li>
                                        <li><i class="icon an an-comments-l"></i><a href="#">5</a>
                                        </li>
                                    </ul>
                                    <div class="rte">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry's standard dummy text ever since the
                                            1500s, when an unknown printer took a galley scrambled it to make specimen
                                            book...</p>
                                    </div>
                                    <p><a href="#" class="btn btn--small rounded">Read more</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endfor

                </div>

                <!--Pagination Classic-->
                <hr class="clear">
                <div class="pagination">
                    <ul>
                        <li class="prev"><a href="#"><i class="icon align-middle an an-caret-left"
                                    aria-hidden="true"></i></a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">...</a></li>
                        <li><a href="#">5</a></li>
                        <li class="next"><a href="#"><i class="icon align-middle an an-caret-right"
                                    aria-hidden="true"></i></a></li>
                    </ul>
                </div>
                <!--End Pagination Classic-->
            </div>
        </div>
        <!--End Main Content-->
    </div>
</div>
@endsection