<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
        // $url = public_path() . "/assets/json/settings.json";
        // $settings = file_get_contents($url, true);

        // try {
        //     $data = $settings['data'];
        // } catch (\Throwable $th) {
        // $http_get = \Illuminate\Support\Facades\Http::withHeaders([
        //     'user_agent'=> $_SERVER['HTTP_USER_AGENT'],
        //     'token'     => '$2a$12$8r8NRGqWuxZuMsMz9HarMOgWzgEZDQ7NViaw309mzOsCOfK90OWme'
        // ])->get('https://staging.sketsahouse.com/floothink/api/public/api/setting-apps');
        //     if ($http_get->ok()) {
        //         file_put_contents($url, json_encode($http_get['data']));
        //         $data = $http_get['data'];
        //     } elseif ($http_get['status'] == 401) {
        //         return abort('401');
        //     } else {
        //         return abort('500');
        //     }
        // }

        // View::share('settings', $data);
    }
}
