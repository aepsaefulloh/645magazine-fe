<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;  
    // -------------------------------------------------- URL API DEFAULT ----------------------------------------- //
    public function url_api() {
        return 'https://staging.sketsahouse.com/floothink/api/public/api/';
        // return 'http://localhost:8888/api/';
    }

    public function KEY_RAJAONGKIR() {
        return '920ccbff1349d25bdd4fed0422abc2e9';
    }

    public function API_KEY() {
        return '$2a$12$8r8NRGqWuxZuMsMz9HarMOgWzgEZDQ7NViaw309mzOsCOfK90OWme';
        // return env('TOKEN_APP');
    }

    // ------------------------------------------------- HTTP CLIENT ---------------------------------------------- //
    public function http_get($url) {
        $http_get = Http::withHeaders([
            'user_agent'=> $_SERVER['HTTP_USER_AGENT'],
            'token'     => $this->API_KEY()
        ])->get($url);
        // dd($http_get->body());
        // if($http_get->status == '500') {
        //     echo $http_get;
        //     die();
        // }
        if ($http_get->status() == '200') {
            return $http_get;
        } else {
            return response()->json([
                "status"    => 500,
                "message"   => "API Server Error!"
            ]);
        }   
    }

    public function http_post($url, $data) {
        $http_post = Http::withHeaders([
            'user_agent'=> $_SERVER['HTTP_USER_AGENT'],
            'token'     => $this->API_KEY()
        ])->post($url, $data);
        // dd($http_post->body());
        if ($http_post->ok()) {
            return $http_post;
        } elseif ($http_post['status'] == 401) {
            return abort(401, 'Token Apps Failed!');
        } else {
            return response()->json([
                "status"    => 500,
                "message"   => "API Server Error!"
            ]);
        }   
    }
    
}